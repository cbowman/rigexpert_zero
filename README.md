# rigexpert_zero
Run SWR analysis on Linux using a RigExpert AA-30.zero

## Usage
You will need to connect your AA-30.zero to a serial interface.

To run a scan, specify the serial interface (frequently /dev/ttyUSB0)
with the -d option, and provide two frequency arguments in Hz (you can
abbreviate arguments like 14000000 to 14e6 to save time).

Avaliable frequency arguments are:
  * center (-c): The center frequency for a scan
  * range (-r): Range for a scan to cover
  * start (-s): Starting frequency
  * end (-e): Ending frequency

Any two frequency arguments will work, for example:
```
re_analyze.py -d /dev/ttyUSB0 -c 14e6 -r 2e6

re_analyze.py -d /dev/ttyUSB0 -s 13e6 -r 2e6

re_analyze.py -d /dev/ttyUSB0 -s 13e6 -e 15e6

```
would all scan from 13 to 15 MHz using an analyzer connected to /dev/ttyUSB0

Once the scan runs, a VSWR plot will be displayed with amateur radio bands
highlighted. You can suppress the plot by passing the --noplot or -n option
(for example, if you want to write to a file).

### File operations
Adding the -o or --writefile option followed by a file name will write sweep
results to a CSV file at the specified path.

To plot the results of a previously saved sweep, omit scan range and device
arguments and instead supply a -f or --readfile argument with a path to a
CSV file from a saved sweep.
