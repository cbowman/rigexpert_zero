import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="rigexpert_zero",
    version="0.0.1",
    author="Craig Bowman",
    author_email="cbbowman.cb@gmail.com",
    description="A tool for running SWR sweeps with the RigExpert AA30.zero on Linux systems",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cbowman/rigexpert_zero/",
    packages=setuptools.find_packages(),
    scripts=['rigexpert_zero/re_analyze.py'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
)
