"""
Control RigExpert AA-30.zero via serial
Craig Bowman K7FLG
"""
import serial

class Analyzer:
    """Send commands and return output"""
    def __init__(self, device):
        self.device = device
        self.ser = serial.Serial(device, 38400, timeout=15)

    def _sercommand(self, command):
        """Send command to RigExpert and return output"""
        self.ser.write((command + "\r\n").encode('ascii'))
        result = self.ser.readline().decode('ascii').strip()
        return result

    def sweep(self, fcenter, frange, nsamples):
        """Sweep a range of frequencies and return analysis data as it is obtained"""
        sweep = []
        rawsweep = []
        self._sercommand("FQ" + str(fcenter))
        self._sercommand("SW" + str(frange))
        self._sercommand("FRX" + str(nsamples))
        for n in range(nsamples):
            rawreading = self.ser.readline().decode('ascii').strip()
            reading = {
                'freq': rawreading.split(',')[0].strip(),
                'resist': rawreading.split(',')[1].strip(),
                'react': rawreading.split(',')[2].strip(),
                'swr': None,
                }
            yield reading


    def range_sweep(self, fstart, fend, nsamples):
        """Sweep given start and end frequencies, yielding analysis data as it is obtained"""
        frange = fend-fstart
        fcenter = fstart + (frange / 2)
        for reading in self.sweep(fcenter, frange, nsamples):
            yield reading

