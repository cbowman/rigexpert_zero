#!/usr/bin/env python3
"""
re_analyze.py

Run sweeps with RigExpert AA-30.zero and plot VSWR

Craig Bowman K7FLG
"""
import argparse
import csv
import time
import math
from rigexpert_zero.aa30zero import Analyzer
from matplotlib import pyplot
from tqdm import tqdm

def read_csv(csvfile):
    """Get readings from standardized csv file"""
    readings = []
    fieldnames = ["freq", "resist", "react", "swr"]
    reader = csv.DictReader(csvfile, fieldnames)
    for line in reader:
        readings.append(line)
    return readings

def write_csv(filename, readings):
    """Write readings to CSV file"""
    fieldnames = ["freq", "resist", "react", "swr"]
    writer = csv.DictWriter(filename, fieldnames)
    for reading in readings:
        writer.writerow(reading)

def calc_swr(resist, react, char_imped=50):
    """Calculate vswr given readings for resistance and reactance (R and X)"""
    resist = float(resist)
    react = float(react)
    char_imped = float(char_imped)
    num = (resist - char_imped)**2 + react**2
    den = (resist + char_imped)**2 + react**2
    gamma = math.sqrt(num / den)
    swr = (1 + gamma) / (1 - gamma)
    return swr

def plot_swr(readings):
    """Plot VSWRs"""
    # General styling
    pyplot.style.use('dark_background')
    pyplot.figure(figsize=(10, 6), dpi=120)

    # Plot Data
    freqs = [float(reading['freq']) for reading in readings]
    swrs = [float(reading['swr']) for reading in readings]
    pyplot.plot(freqs, swrs, c='w')

    # SWR 2 line
    pyplot.axhline(y=2, ls='dashed', c='y')

    # Labels
    pyplot.title('Antenna SWR Analysis')
    pyplot.ylabel('VSWR')
    pyplot.xlabel('Frequency (MHz)')

    # Axis scaling
    if max(swrs) > 15:
        pyplot.ylim(bottom=0, top=15)
    else:
        pyplot.ylim(bottom=0)

    # Bands
    bands = (
        (160, 1.8, 2.0),
        (80, 3.5, 4.0),
        (60, 5.33, 5.405),
        (40, 7.0, 7.3),
        (30, 10.1, 10.15),
        (20, 14.0, 14.35),
        (17, 18.068, 18.168),
        (15, 21.0, 21.45),
        (12, 24.89, 24.99),
        (10, 28.0, 29.7)
    )
    for band in bands:
        if (band[1] >= freqs[0] and band[2] <= freqs[-1]):
            pyplot.axvspan(band[1], band[2], color='blue', alpha=0.25, label=band[0])
    pyplot.show()

def configure_parser():
    """Set up argument parser"""
    description = "run analysis and plot SWR with RigExpert AA-30.Zero via serial interface"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--noplot', '-n', action='store_true',
                        help='Skip showing a plot, just run sweep and write file (if specified)')
    parser.add_argument('--version', '-v', action='store_true',
                        help='Show version info')

    scan = parser.add_argument_group('scanning', 'commands for scanning a frequency range')
    scan.add_argument('--start', '-s', nargs='?', type=float,
                        help='Starting frequency for analysis')
    scan.add_argument('--end', '-e', nargs='?', type=float,
                        help='Ending frequency for analysis')
    scan.add_argument('--center', '-c', nargs='?', type=float,
                        help='Center frequency for analysis')
    scan.add_argument('--range', '-r', nargs='?', type=float,
                        help='Frequency range for analysis')
    scan.add_argument('--resolution', '-R', nargs='?', type=int, default=100,
                        help='Make <points> measurements')
    scan.add_argument('--device', '-d', nargs='?',
                        help='Device for RigExpert connection')

    files = parser.add_argument_group('file operations', 'read and write scan information in CSV files')
    files.add_argument('--readfile', '-f', nargs='?', type=argparse.FileType('r'),
                        help='Read sweep information from file and show plot (do not run a scan)')
    files.add_argument('--writefile', '-o', nargs='?', type=argparse.FileType('w'),
                        help='Write sweep information to file')

    args = parser.parse_args()

    # Validate arguments
    if not (args.readfile or args.device):
        parser.error('must specify a file to read or a scanning device')
    if args.readfile and args.device:
        parser.error('can\'t run a scan and read information from a file simultaneously')
    if args.device and not (args.start or args.end or args.center or args.range):
        parser.error('must specify range parameters (start/end/center/range) to use a device')

    # Translate range arguments to center and range, or give an error
    if args.device:
        if args.start and args.center:
            args.range = 2*(args.center - args.start)
        elif args.start and args.range:
            args.center = args.start + (args.range / 2)
        elif args.start and args.end:
            args.range = args.end - args.start
            args.center = args.start + (args.range / 2)
        elif args.center and args.end:
            args.range = 2*(args.end - args.center)
        elif args.end and args.range:
            args.center = args.end - (args.range / 2)
        elif not (args.center and args.range):
            parser.error('must specify two range parameters (start/end/center/range)')

    return args

def main():
    args = configure_parser()
    if args.readfile:
        readings = read_csv(args.readfile)
    if args.device:
        analyzer = Analyzer(args.device)
        readings = []
        print("Running sweep...")
        for reading in tqdm(analyzer.sweep(args.center, args.range, args.resolution),
                            total=args.resolution, unit="readings"):
            readings.append(reading)
    for reading in readings:
        if not reading["swr"]:
            reading["swr"] = calc_swr(reading["resist"], reading["react"])

    if not args.noplot:
        plot_swr(readings)
    if args.writefile:
        write_csv(args.writefile, readings)

if __name__ == "__main__":
    main()
